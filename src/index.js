'use strict';

/**
 * LanguageConfidence
 */
export class LanguageConfidence {

    /**
     * @param {Config} [options] - default options for interacting with the LanguageConfidence service
     */
    constructor(options) {
        options = (typeof options === "object") ? options : {};

        // todo maybe setup a default api-key and server-url that allow out of the box testing (throttled or limited API, etc)

        this.serverUrl = (typeof options.serverUrl === "string")
            ? options.serverUrl : "https://api.languageconfidence.ai/v1";

        this.accessToken = (typeof options.accessToken === "string")
            ? options.accessToken : "";

        this.difficulty = (typeof options.difficulty === "string")
            ? options.difficulty : "intermediate";

        this.phonemeSystem = (typeof options.phonemeSystem === "string")
            ? options.phonemeSystem : "ipa";
    }

    /**
     * Get the pronunciation scores for a audio recording.
     *
     * @param {ScoreRequest} [request] - the request to send to the server
     * @return {Promise<ScoreResponse>} the ScoreResponse from the server as a Promise
     */
    getPronunciationScore(request) {

        let difficulty = request.difficulty ? request.difficulty : this.difficulty;
        let phonemeSystem = request.phonemeSystem ? request.phonemeSystem : this.phonemeSystem;
        let accessToken = request.accessToken ? request.accessToken : this.accessToken;

        let endPoint = this.serverUrl + '/pronunciation' + (phonemeSystem === 'cmu' ? 'cmu' : '');

        return fetch(endPoint, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + accessToken
            },
            body: JSON.stringify({
                "format": request.format,
                "content": request.content,
                "audioBase64": request.audioBase64
            })
        })
        .then(r => {
            if (r.status >= 200 && r.status < 300) {
                return Promise.resolve(r);
            } else {
                return r.json().then(e => {
                    let error;
                    if (e.fault && e.fault.detail && e.fault.detail.errorcode) {
                        error = new Error(e.fault.detail.errorcode);
                    } else {
                        error = new Error(r.statusText);
                    }
                    error['status'] = r.status;
                    error['detail'] = e;
                    return Promise.reject(error);
                });
            }
        })
        .then(r => r.json())
        .then(data => {
            return Promise.resolve(buildResponse(data, request.content, difficulty, phonemeSystem));
        });
    }

    /**
     * Calculate gradings for the score based on a new difficulty level. The original score is left unchanged and
     * a new score response is returned with the adjusted grades included.
     *
     * @param {ScoreResponse} [score] - the original score response to be adjusted
     * @param {string} [newDifficulty] - the new difficulty ('beginner', 'intermediate', 'advanced') to use for grading
     * @return {Promise<ScoreResponse>} the adjusted ScoreResponse with new grades as a Promise
     */
    adjustPronunciationScoreGrades(score, newDifficulty) {
        return Promise.resolve(
            buildResponse(
                score.getRawOutput(),
                score.getSentenceScore().label,
                newDifficulty,
                score.getPhonemeSystem()
            ));
    }
}


/**
 * ScoreResponse
 */
export class ScoreResponse {

    /**
     * @ignore
     *
     * Internal constructor used for setting the raw data from the server and it's processed values.
     *
     * @param {ScoreData} rawOutput - the score data received from the server
     * @param {Object} _processed - the processed data for this response.
     */
    _init(rawOutput, _processed) {
        this._rawOutput = rawOutput;
        this._processed = _processed;
    }

    /**
     * @returns {string} the difficulty that was used for grading scores
     */
    getDifficulty() {
        return this._processed.difficulty;
    }

    /**
     * @returns {string} the phoneme system that was used when processing the audio
     */
    getPhonemeSystem() {
        return this._processed.phonemeSystem;
    }

    /**
     * @returns {ScoreData} the raw data (unprocessed) that was returned by the server
     */
    getRawOutput() {
        return this._rawOutput;
    }

    /**
     * @returns {SentenceScore} the overall score for the sentence
     */
    getSentenceScore() {
        return this._processed.sentenceScore;
    }

    /**
     * @returns {WordScore[]} an array of word scores, one for each word in the sentence
     */
    getWordScoresList() {
        return this._processed.wordScores;
    }

    /**
     * @returns {WordScore[]} an array of word scores, one for each time the word occurred (in order it occurred)
     * in the sentence.
     */
    getWordScores(word) {
        return this._processed.wordScores
            ? this._processed.wordScores.filter(w => w.label === word)
            : null;
    }

    /**
     * @returns {PhonemeScore[][]} for each word time the word occurred
     */
    getPhonemeScoresList(word) {
        return this._processed.phonemeScoresByWord[word];
    }

    /**
     * @returns {PhonemeScore[]}
     */
    getPhonemeScores(phoneme) {
        return this._processed.phonemeScores
            ? this._processed.phonemeScores.filter(p => p.label === phoneme)
            : null;
    }

}

//-----------------------------------------------------------------------------
// Type Definitions

/**
 * @class {Object} Config
 * @property {string} acccessToken - your access_token for securely using the API (obtained via a call to
 * https://api.languageconfidence.ai/oauth/access_token)
 * @property {string} [serverUrl] - the URL of the LanguageConfidence server to call. Defaults to
 * 'https://api.languageconfidence.ai/vi'.
 * @property {string} [difficulty] - the default difficulty ('beginner', 'intermediate', 'advanced') to use for gradings.
 * Defaults to 'intermediate'.
 * @property {string} phonemeSystem - the default phonemeSystem ('ipa' or 'cmu') to use. This can be overridden for each
 * request. Defaults to 'ipa'.
 */


/**
 * @typedef {Object} ScoreRequest
 * @property {string} format - the format of the audio file being uploaded (must be either 'wav' or 'mp3')
 * @property {string} content - the text phrase that the audio file is a recording for
 * @property {string} audioBase64 - the raw data of the audio file (either 'wav' or 'mp3') in base 64 encoding
 * @property {string} [phonemeSystem] - the phoneme system ('ipa' or 'cmu') to use for processing this request.
 * If not provided, the default value provided in the constructor will be used.
 * @property {string} [difficulty] - the difficulty ('beginner', 'intermediate', 'advanced') to use for gradings.
 * If not provided, the default value provided in the constructor will be used.
 * @property {string} [apiKey] - your access_token for securely using the API (obtained via a call to
 * https://api.languageconfidence.ai/oauth/access_token). If not provided, the default value provided in the
 * constructor will be used.
 */
/**
 * @typedef {Object} ScoreData
 * @property {WordData[]} word_list - todo
 * @property {number} sentence_mean - todo
 */

/**
 * @typedef {Object} WordData
 * @property {PhonemeData[]} phonemes - todo
 * @property {number} time_start - todo
 * @property {number} time_end - todo
 * @property {number} label - todo
 * @property {number} mean - todo
 */

/**
 * @typedef {Object} PhonemeData
 * @property {SoundsLikeData[]} sounds_like - todo
 * @property {number} time_start - todo
 * @property {number} time_end - todo
 * @property {number} label - todo
 * @property {number} mean - todo
 */

/**
 * @typedef {Object[]} SoundsLikeData
 */

/**
 * @typedef {Object} SentenceScore
 * @property {string} label - todo
 * @property {number} rawScore - todo
 * @property {number} gradedScore - todo
 * @property {number} integerScore - todo
 * @property {number} timestamp - todo
 */

/**
 * @typedef {Object} WordScore
 * @property {string} label - todo
 * @property {string} rawScore - todo
 * @property {number} gradedScore - todo
 * @property {number} integerScore - todo
 * @property {number} timestamp - todo
 */

/**
 * @typedef {Object} PhonemeScore
 * @property {string} label - todo
 * @property {number} rawScore - todo
 * @property {string} gradedScore - todo
 * @property {number} integerScore - todo
 * @property {number} timestamp - todo
 * @property {SoundedLike[]} soundedLike - todo
 */

/**
 * @typedef {string[]} SoundedLike
 */

//-----------------------------------------------------------------------------
// Private functions

function buildResponse(dataFromServer, sentence, difficulty, phonemeSystem) {

    let sentenceLabel = sentence.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"").toUpperCase();

    let _processed = {
        difficulty: difficulty,
        phonemeSystem: phonemeSystem,
        sentenceScore: {
            label: sentenceLabel,
            rawScore: dataFromServer.sentence_mean,
            gradedScore: getGradedScore(difficulty, dataFromServer.sentence_mean),
            integerScore: 'todo',
            timestamp: null // todo should there be time data on the sentence level?
        },
        wordScores: [],
        phonemeScores: [],
        phonemeScoresByWord: {},
        soundsLikeScoresByPhoneme: {}
    };

    if (dataFromServer.word_list) {
        dataFromServer.word_list.forEach(wordData => {
            processWord(wordData, _processed);
        });
    }

    let result = new ScoreResponse();
    result._init(dataFromServer, _processed);
    return result;
}

function processWord(wordData, _processed) {
    _processed.wordScores.push({
        label: wordData.label,
        rawScore: wordData.mean,
        gradedScore: getGradedScore(_processed.difficulty, wordData.mean),
        integerScore: 'todo',
        timestamp: [wordData.time_start, wordData.time_end]
    });

    processPhonemes(wordData, _processed);
}

function processPhonemes(wordData, _processed) {

    if (wordData.phonemes && wordData.phonemes.length) {
        _processed.phonemeScoresByWord[wordData.label]
            = _processed.phonemeScoresByWord[wordData.label]
            ? _processed.phonemeScoresByWord[wordData.label] : [];

        let phonemeScoresForWord = [];

        wordData.phonemes.forEach(phonemeData => {

            let phonemeScore = {
                label: phonemeData.label,
                rawScore: phonemeData.score,
                gradedScore: getGradedScore(_processed.difficulty, phonemeData.score),
                integerScore: 'todo',
                timestamp: [phonemeData.time_start, phonemeData.time_end],
                soundedLike: phonemeData.sounds_like
            };
            _processed.phonemeScores.push(phonemeScore);
            phonemeScoresForWord.push(phonemeScore);
        });

        _processed.phonemeScoresByWord[wordData.label].push(phonemeScoresForWord);
    }
}

function getGradedScore(difficulty, value) {
    switch (difficulty) {
        case 'beginner':
            return value < 0.10 ? 'poor' : value < 0.2 ? 'average' : 'good';
        case 'intermediate':
            return value < 0.20 ? 'poor' : value < 0.3 ? 'average' : 'good';
        case 'advanced':
            return value < 0.30 ? 'poor' : value < 0.4 ? 'average' : 'good';
    }
}
