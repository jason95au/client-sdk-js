export default {
    "word_list":
        [
            {
                "phonemes": [
                    {
                        "sounds_like": [["SIL", 0.9621], ["aɪ", 0.0106], ["spn", 0.0096], ["æ", 0.0089], ["θ", 0.0015]],
                        "score": 0.9621,
                        "time_start": 0,
                        "time_end": 0.38,
                        "label": "SIL"
                    }
                ],
                "time_start": 0,
                "time_end": 0.38,
                "label": "<eps>",
                "mean": null
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["æ", 0.8053], ["aɪ", 0.0641], ["SIL", 0.0507], ["h", 0.0311], ["f", 0.0152]],
                        "score": 0.8053,
                        "time_start": 0.38,
                        "time_end": 0.59,
                        "label": "æ"
                    },
                    {
                        "sounds_like": [["f", 0.6652], ["æ", 0.1981], ["θ", 0.0628], ["v", 0.0255], ["s", 0.0148]],
                        "score": 0.6652,
                        "time_start": 0.59,
                        "time_end": 0.67,
                        "label": "f"
                    },
                    {
                        "sounds_like": [["f", 0.3574], ["θ", 0.2139], ["ɹ", 0.1278], ["ɝ", 0.126], ["t", 0.0745]],
                        "score": 0.0745,
                        "time_start": 0.67,
                        "time_end": 0.71,
                        "label": "t"
                    },
                    {
                        "sounds_like": [["ɝ", 0.3572], ["ʌ", 0.3471], ["aɪ", 0.1094], ["ɹ", 0.0296], ["æ", 0.0257]],
                        "score": 0.3572,
                        "time_start": 0.71,
                        "time_end": 0.8,
                        "label": "ɝ"
                    }
                ],
                "time_start": 0.38,
                "time_end": 0.8,
                "label": "AFTER",
                "mean": 0.47555
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["d", 0.7808], ["ʌ", 0.1038], ["t", 0.0393], ["aɪ", 0.0096], ["θ", 0.0094]],
                        "score": 0.0087,
                        "time_start": 0.8,
                        "time_end": 0.9,
                        "label": "ð"
                    },
                    {
                        "sounds_like": [["ʌ", 0.4781], ["ɑ", 0.1007], ["s", 0.0761], ["d", 0.058], ["ɛ", 0.0579]],
                        "score": 0.4781,
                        "time_start": 0.9,
                        "time_end": 1.01,
                        "label": "ʌ"
                    }
                ],
                "time_start": 0.8,
                "time_end": 1.01,
                "label": "THE",
                "mean": 0.2434
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["s", 0.6441], ["ʃ", 0.1412], ["ʌ", 0.0771], ["SIL", 0.0268], ["t", 0.019]],
                        "score": 0.1412,
                        "time_start": 1.01,
                        "time_end": 1.24,
                        "label": "ʃ"
                    },
                    {
                        "sounds_like": [["oʊ", 0.5788], ["ɑ", 0.2026], ["ɔ", 0.0562], ["ʌ", 0.0481], ["SIL", 0.0301]],
                        "score": 0.5788,
                        "time_start": 1.24,
                        "time_end": 1.79,
                        "label": "oʊ"
                    }
                ],
                "time_start": 1.01,
                "time_end": 1.79,
                "label": "SHOW",
                "mean": 0.36
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["SIL", 0.7593], ["oʊ", 0.2029], ["w", 0.0278], ["h", 0.0021], ["ʌ", 0.0016]],
                        "score": 0.7593,
                        "time_start": 1.79,
                        "time_end": 1.82,
                        "label": "SIL"
                    }
                ],
                "time_start": 1.79,
                "time_end": 1.82,
                "label": "<eps>",
                "mean": null
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["w", 0.9647], ["SIL", 0.0236], ["i", 0.0077], ["l", 0.0012], ["ɹ", 0.0008]],
                        "score": 0.9647,
                        "time_start": 1.82,
                        "time_end": 1.92,
                        "label": "w"
                    },
                    {
                        "sounds_like": [["i", 0.9687], ["w", 0.0305], ["ɪ", 0.0002], ["eɪ", 0.0002], ["ɛ", 0.0001]],
                        "score": 0.9687,
                        "time_start": 1.92,
                        "time_end": 2.01,
                        "label": "i"
                    }
                ],
                "time_start": 1.82,
                "time_end": 2.01,
                "label": "WE",
                "mean": 0.9667
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["w", 0.9433], ["i", 0.0456], ["ɛ", 0.011], ["h", 0], ["spn", 0]],
                        "score": 0.9433,
                        "time_start": 2.01,
                        "time_end": 2.11,
                        "label": "w"
                    },
                    {
                        "sounds_like": [["ɛ", 0.9282], ["n", 0.0481], ["w", 0.0233], ["ɪ", 0.0005], ["ʌ", 0]],
                        "score": 0.9282,
                        "time_start": 2.11,
                        "time_end": 2.19,
                        "label": "ɛ"
                    },
                    {
                        "sounds_like": [["n", 0.9907], ["t", 0.0086], ["ɛ", 0.0007], ["ɪ", 0], ["ŋ", 0]],
                        "score": 0.9907,
                        "time_start": 2.19,
                        "time_end": 2.25,
                        "label": "n"
                    },
                    {
                        "sounds_like": [["t", 0.8288], ["n", 0.1711], ["θ", 0], ["d", 0], ["p", 0]],
                        "score": 0.8288,
                        "time_start": 2.25,
                        "time_end": 2.29,
                        "label": "t"
                    }
                ],
                "time_start": 2.01,
                "time_end": 2.29,
                "label": "WENT",
                "mean": 0.92275
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["t", 0.962], ["u", 0.0233], ["ʌ", 0.0145], ["ɪ", 0.0001], ["θ", 0]],
                        "score": 0.962,
                        "time_start": 2.29,
                        "time_end": 2.36,
                        "label": "t"
                    },
                    {
                        "sounds_like": [["u", 0.8089], ["ʌ", 0.0952], ["ɑ", 0.0307], ["ɔ", 0.0203], ["ɔɪ", 0.0096]],
                        "score": 0.8089,
                        "time_start": 2.36,
                        "time_end": 2.55,
                        "label": "u"
                    }
                ],
                "time_start": 2.29,
                "time_end": 2.55,
                "label": "TO",
                "mean": 0.88545
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["aʊ", 0.6197], ["ɹ", 0.1827], ["ɑ", 0.0401], ["l", 0.0392], ["ʌ", 0.0321]],
                        "score": 0.0321,
                        "time_start": 2.55,
                        "time_end": 2.72,
                        "label": "ʌ"
                    }
                ],
                "time_start": 2.55,
                "time_end": 2.72,
                "label": "A",
                "mean": 0.0321
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["w", 0.864], ["ɛ", 0.0613], ["ɹ", 0.026], ["ɡ", 0.0099], ["b", 0.0087]],
                        "score": 0.026,
                        "time_start": 2.72,
                        "time_end": 2.86,
                        "label": "ɹ"
                    },
                    {
                        "sounds_like": [["ɛ", 0.9745], ["spn", 0.0116], ["ɪ", 0.0063], ["s", 0.0022], ["eɪ", 0.0014]],
                        "score": 0.9745,
                        "time_start": 2.86,
                        "time_end": 2.92,
                        "label": "ɛ"
                    },
                    {
                        "sounds_like": [["s", 0.9503], ["t", 0.036], ["spn", 0.0056], ["ɛ", 0.0047], ["z", 0.002]],
                        "score": 0.9503,
                        "time_start": 2.92,
                        "time_end": 3.01,
                        "label": "s"
                    },
                    {
                        "sounds_like": [["t", 0.7397], ["ɝ", 0.1747], ["ʌ", 0.0636], ["s", 0.0069], ["oʊ", 0.0049]],
                        "score": 0.7397,
                        "time_start": 3.01,
                        "time_end": 3.08,
                        "label": "t"
                    },
                    {
                        "sounds_like": [["w", 0.5056], ["ɝ", 0.3951], ["ʌ", 0.0751], ["oʊ", 0.0111], ["ɹ", 0.0036]],
                        "score": 0.3951,
                        "time_start": 3.08,
                        "time_end": 3.2,
                        "label": "ɝ"
                    },
                    {
                        "sounds_like": [["ɑ", 0.3764], ["w", 0.3684], ["ʌ", 0.2022], ["ɔ", 0.0241], ["aɪ", 0.0124]],
                        "score": 0.3764,
                        "time_start": 3.2,
                        "time_end": 3.34,
                        "label": "ɑ"
                    },
                    {
                        "sounds_like": [["n", 0.8944], ["ɑ", 0.0426], ["ʌ", 0.0343], ["t", 0.0096], ["ɔ", 0.0076]],
                        "score": 0.8944,
                        "time_start": 3.34,
                        "time_end": 3.45,
                        "label": "n"
                    },
                    {
                        "sounds_like": [["n", 0.8813], ["SIL", 0.0432], ["t", 0.0264], ["spn", 0.0149], ["ŋ", 0.0088]],
                        "score": 0.0264,
                        "time_start": 3.45,
                        "time_end": 3.48,
                        "label": "t"
                    }
                ],
                "time_start": 2.72,
                "time_end": 3.48,
                "label": "RESTAURANT",
                "mean": 0.21
            },
            {
                "phonemes": [
                    {
                        "sounds_like": [["SIL", 0.8898], ["n", 0.0961], ["t", 0.0049], ["spn", 0.002], ["ŋ", 0.002]],
                        "score": 0.8898,
                        "time_start": 3.48,
                        "time_end": 3.51,
                        "label": "SIL"
                    }
                ],
                "time_start": 3.48,
                "time_end": 3.51,
                "label": "<eps>",
                "mean": null
            }
        ],
    "sentence_mean": 0.554225
}