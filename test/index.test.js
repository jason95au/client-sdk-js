import {LanguageConfidence} from "../src/index.js";
import data_req from "./data/audio1-wav.js";
import data_resp from "./data/response1.js";

global.fetch = require('jest-fetch-mock');

test('Constructs a new LanguageConfidence instance with defaults', () => {

    let lc = new LanguageConfidence({
        accessToken: 'access-token',
    });

    expect(lc.accessToken).toBe('access-token');
    expect(lc.serverUrl).toBe('https://api.languageconfidence.ai/v1');
    expect(lc.difficulty).toBe('intermediate');
    expect(lc.phonemeSystem).toBe('ipa');
});

test('Constructs a new LanguageConfidence instance with values', () => {

    let lc = new LanguageConfidence({
        accessToken: 'access-token',
        serverUrl: 'server-url',
        difficulty: 'beginner',
        phonemeSystem: 'cmu'
    });

    expect(lc.accessToken).toBe('access-token');
    expect(lc.serverUrl).toBe('server-url');
    expect(lc.difficulty).toBe('beginner');
    expect(lc.phonemeSystem).toBe('cmu');
});

test('Test sentence score', (done) => {

    let lc = new LanguageConfidence({
        accessToken: 'access-token'
    });

    fetch.mockResponse(JSON.stringify(data_resp));

    lc.getPronunciationScore(data_req).then(r => {

        let sentenceScore = r.getSentenceScore();
        expect(sentenceScore.label).toBe('AFTER THE SHOW WE WENT TO A RESTAURANT');
        expect(sentenceScore.rawScore).toBe(0.554225);
        expect(sentenceScore.gradedScore).toBe('good');

        done();
    });
});

test('Test word scores list', (done) => {

    let lc = new LanguageConfidence({
        accessToken: 'access-token'
    });

    fetch.mockResponse(JSON.stringify(data_resp));

    lc.getPronunciationScore(data_req).then(r => {

        let wordScores = r.getWordScoresList();
        expect(wordScores.length).toBe(11);

        let wordScore = wordScores[1];
        expect(wordScore.label).toBe('AFTER');
        expect(wordScore.rawScore).toBe(0.47555);
        expect(wordScore.gradedScore).toBe('good');

        wordScore = wordScores[9];
        expect(wordScore.label).toBe('RESTAURANT');
        expect(wordScore.rawScore).toBe(0.21);
        expect(wordScore.gradedScore).toBe('average');

        done();
    });
});

test('Test individual word scores', (done) => {

    let lc = new LanguageConfidence({
        accessToken: 'access-token'
    });

    fetch.mockResponse(JSON.stringify(data_resp));

    lc.getPronunciationScore(data_req).then(r => {

        let wordScores = r.getWordScores('AFTER');
        expect(wordScores.length).toBe(1);

        let wordScore = wordScores[0];
        expect(wordScore.label).toBe('AFTER');
        expect(wordScore.rawScore).toBe(0.47555);
        expect(wordScore.gradedScore).toBe('good');


        wordScores = r.getWordScores('RESTAURANT');
        expect(wordScores.length).toBe(1);

        wordScore = wordScores[0];
        expect(wordScore.label).toBe('RESTAURANT');
        expect(wordScore.rawScore).toBe(0.21);
        expect(wordScore.gradedScore).toBe('average');

        done();
    });
});
//
// test('Test word scores list', (done) => {
//
//     let lc = new LanguageConfidence({
//         accessToken: 'access-token'
//     });
//
//     fetch.mockResponse(JSON.stringify(data_resp));
//
//     lc.getPronunciationScore(data_req).then(r => {
//
//         let wordScores = r.getWordScoresList();
//         expect(wordScores.length).toBe(11);
//
//         let wordScore = wordScores[1];
//         expect(wordScore.label).toBe('AFTER');
//         expect(wordScore.rawScore).toBe(0.47555);
//         expect(wordScore.gradedScore).toBe('good');
//
//         wordScore = wordScores[9];
//         expect(wordScore.label).toBe('RESTAURANT');
//         expect(wordScore.rawScore).toBe(0.21);
//         expect(wordScore.gradedScore).toBe('average');
//
//         done();
//     });
// });
//
// test('Test individual word scores', (done) => {
//
//     let lc = new LanguageConfidence({
//         accessToken: 'access-token'
//     });
//
//     fetch.mockResponse(JSON.stringify(data_resp));
//
//     lc.getPronunciationScore(data_req).then(r => {
//
//         let wordScores = r.getWordScores('AFTER');
//         expect(wordScores.length).toBe(1);
//
//         let wordScore = wordScores[0];
//         expect(wordScore.label).toBe('AFTER');
//         expect(wordScore.rawScore).toBe(0.47555);
//         expect(wordScore.gradedScore).toBe('good');
//
//
//         wordScores = r.getWordScores('RESTAURANT');
//         expect(wordScores.length).toBe(1);
//
//         wordScore = wordScores[0];
//         expect(wordScore.label).toBe('RESTAURANT');
//         expect(wordScore.rawScore).toBe(0.21);
//         expect(wordScore.gradedScore).toBe('average');
//
//         done();
//     });
// });
