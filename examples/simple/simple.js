import {LanguageConfidence} from "../../dist/bundle.js";
import data_audio1Wav from "../data/audio1-wav.js";
import data_audio1Mp3 from "../data/audio1-mp3.js";

$(document).ready(function () {

    $("#input-form").submit(function (e) {
        e.preventDefault();

        let $message = $("#message");
        let $resultDetails = $("#result-details");

        let langConf = new LanguageConfidence({
            accessToken: $('#accessToken').val()
        });

        let type = $("#selectData").val();
        let request = type === 'audio1-wav' ? data_audio1Wav : data_audio1Mp3;

        $resultDetails.hide();
        $message.show();
        $message.text('Loading...');

        langConf.getPronunciationScore(request)
            .then( result => {

                console.log('Response is', result);

                $message.hide();
                $resultDetails.show();

                let sentence = result.getSentenceScore();

                $('#sentenceRawScore').text(sentence.rawScore);
                $('#sentenceGradedScore').text(sentence.gradedScore);

                let words = result.getSentenceScore().label.split(" ");
                let word = words[Math.floor(Math.random() * words.length)];
                let wordScore = result.getWordScores(word)[0];

                $('#wordLabel').text(wordScore.label);
                $('#wordRawScore').text(wordScore.rawScore);
                $('#wordGradedScore').text(wordScore.gradedScore);

                let phonemeScoresList = result.getPhonemeScoresList('SHOW');
                console.log('Phoneme Score List', phonemeScoresList);
                phonemeScoresList[0].forEach(phonemeScore => {
                    console.log('Phoneme Score', phonemeScore);
                    phonemeScore.soundedLike.forEach(like => {
                        console.log(' - Sounded like', like);
                    })
                });

                let phonemeScores_OU = result.getPhonemeScores('oʊ');
                console.log('Scores for oʊ', phonemeScores_OU);
                phonemeScores_OU.forEach(phonemeScore => {
                    console.log('Phoneme Score', phonemeScore);
                    phonemeScore.soundedLike.forEach(like => {
                        console.log(' - Sounded like', like);
                    })
                });
            })
            .catch( error => {
                console.log('Error Message: ' + error.message);
                console.log('Error Status: ' + error.status);
                console.log('Error Detail: ', error.detail);
                $message.html('<p style="color: red">Error: '  + error.message + '</p>');
            });

        return false;
    });
});